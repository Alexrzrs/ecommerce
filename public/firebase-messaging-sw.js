importScripts(
    "https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js",
    "https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js"
);

const firebaseConfig = {
    apiKey: "AIzaSyCZdmXEtClZuU5nZJaa1nb3cT3C5XfCCIU",
    authDomain: "ecommerce-acf03.firebaseapp.com",
    projectId: "ecommerce-acf03",
    storageBucket: "ecommerce-acf03.appspot.com",
    messagingSenderId: "211023368893",
    appId: "1:211023368893:web:03d08ab7a539351e8d9dd4",
    measurementId: "G-T4VNE7WBVK",
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage((payload) => {
    // console.log("Recibiendo msg en segundo plano");
    const tituloNotificacion = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: payload.notification.icon,
    };
    self.registration.showNotification(tituloNotificacion, options);
});
