Titulo
(Titulo de la solicitud)

Resumen:
(Breve descripción de la documentación o tutorial que estás solicitando)

Descripción:
(Explica por qué esta documentación o tutorial beneficia al proyecto y cómo ayudaría a los usuarios)

Área Específica:
(Especifica la sección o el aspecto del proyecto para el cual necesitas documentación o un tutorial)

Objetivo:
(Detalla el propósito principal de la documentación o tutorial)

Beneficios para los Usuarios:
(Explica cómo los usuarios se beneficiarían de la información proporcionada)

Contenido Sugerido:
(Proporciona sugerencias sobre el contenido que te gustaría ver en la documentación o tutorial)

Formato Preferido:
(Indica si prefieres un documento escrito o un video tutorial)

Capturas de Pantalla o Ejemplos:
(Si es aplicable, proporciona capturas de pantalla o ejemplos que puedan ayudar a entender mejor el contenido)

¿Existe documentación existente que se pueda ampliar o mejorar?
(Informa sobre cualquier documentación existente y si esta solicitud es una extensión o mejora de la misma)

Información Adicional:
(Añade cualquier información adicional que creas que sea relevante)
