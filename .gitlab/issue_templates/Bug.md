Titulo
(Titulo del error)

Resumen
(Resuma de manera concisa el error encontrado)

Pasos para reproducir
(Cómo se puede reproducir el problema, esto es muy importante)

¿Cuál es el comportamiento actual del error?
(Lo que realmente sucede)

¿Cuál es el comportamiento esperado correcto?
(Lo que debería ver en su lugar)

Registros y/o capturas de pantalla relevantes
(Adjunte imagenes aqui)

Frequencia
(Veces que ocurre el bug)

Datos del sistema
(Sistema operativo, navegador, version del navegador, version de la aplicacion)
