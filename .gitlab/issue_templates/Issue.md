Titulo
(Titulo del problema)

Resumen:
(Breve descripción del problema que has encontrado)

Descripción:
(Proporciona una explicación detallada del problema, incluyendo cualquier información relevante sobre el entorno y las circunstancias en las que ocurrió)

Pasos para Reproducir:
(Lista los pasos específicos necesarios para reproducir el problema, si es posible)

Comportamiento Actual:
(Describe detalladamente el comportamiento incorrecto o inesperado que estás experimentando)

Comportamiento Esperado:
(Explica cómo debería comportarse el sistema en lugar del comportamiento actual)

Impacto:
(Discute el impacto del problema en el proyecto, usuarios u otros componentes)

Fecha y Hora de Ocurrencia:
(Proporciona la fecha y hora aproximadas en que ocurrió el problema)

Entorno:
(Incluye información sobre el entorno en el que se encontraba cuando ocurrió el problema, como sistema operativo, navegador, versión del software, etc.)

Capturas de Pantalla o Archivos Adjuntos:
(Si es aplicable, adjunta capturas de pantalla o archivos que ayuden a entender o reproducir el problema)

Pasos de Mitigación (si los hay):
(Si conoces alguna acción temporal para mitigar el problema, proporciónala aquí)

Prioridad:
(Establece la prioridad del problema en una escala, si es posible)

¿Estás trabajando en una solución?:
(Indica si ya estás abordando el problema o si necesitas la colaboración de otros para resolverlo)

Información Adicional:
(Añade cualquier información adicional que creas que sea relevante)
