export const db = [
    { "id": 1, "precio": 883, "titulo": "XK", "descripcion": "Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.", "stock": 95, "imagen": "http://dummyimage.com/112x100.png/cc0000/ffffff", "categoria": "Jaguar" },
    { "id": 2, "precio": 722, "titulo": "Prelude", "descripcion": "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.", "stock": 31, "imagen": "http://dummyimage.com/166x100.png/ff4444/ffffff", "categoria": "Honda" },
    { "id": 3, "precio": 172, "titulo": "RSX", "descripcion": "In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.", "stock": 48, "imagen": "http://dummyimage.com/146x100.png/cc0000/ffffff", "categoria": "Acura" },
    { "id": 4, "precio": 838, "titulo": "Silverado", "descripcion": "Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.", "stock": 24, "imagen": "http://dummyimage.com/226x100.png/dddddd/000000", "categoria": "Chevrolet" },
    { "id": 5, "precio": 872, "titulo": "Tracer", "descripcion": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.", "stock": 97, "imagen": "http://dummyimage.com/127x100.png/dddddd/000000", "categoria": "Mercury" },
    { "id": 6, "precio": 286, "titulo": "Canyon", "descripcion": "Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.", "stock": 87, "imagen": "http://dummyimage.com/128x100.png/5fa2dd/ffffff", "categoria": "GMC" },
    { "id": 7, "precio": 214, "titulo": "xB", "descripcion": "In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.", "stock": 58, "imagen": "http://dummyimage.com/228x100.png/cc0000/ffffff", "categoria": "Scion" },
    { "id": 8, "precio": 567, "titulo": "A3", "descripcion": "Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.", "stock": 55, "imagen": "http://dummyimage.com/121x100.png/cc0000/ffffff", "categoria": "Audi" },
    { "id": 9, "precio": 699, "titulo": "Electra", "descripcion": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.", "stock": 46, "imagen": "http://dummyimage.com/233x100.png/cc0000/ffffff", "categoria": "Buick" },
    { "id": 10, "precio": 742, "titulo": "Catera", "descripcion": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.", "stock": 16, "imagen": "http://dummyimage.com/153x100.png/5fa2dd/ffffff", "categoria": "Cadillac" },
    { "id": 11, "precio": 383, "titulo": "Silverado 1500", "descripcion": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.", "stock": 58, "imagen": "http://dummyimage.com/176x100.png/cc0000/ffffff", "categoria": "Chevrolet" },
    { "id": 12, "precio": 406, "titulo": "MX-5", "descripcion": "Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.", "stock": 90, "imagen": "http://dummyimage.com/167x100.png/cc0000/ffffff", "categoria": "Mazda" },
    { "id": 13, "precio": 525, "titulo": "Lancer Evolution", "descripcion": "Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.", "stock": 92, "imagen": "http://dummyimage.com/213x100.png/5fa2dd/ffffff", "categoria": "Mitsubishi" },
    { "id": 14, "precio": 552, "titulo": "Dakota", "descripcion": "Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.", "stock": 57, "imagen": "http://dummyimage.com/194x100.png/ff4444/ffffff", "categoria": "Dodge" },
    { "id": 15, "precio": 338, "titulo": "Civic GX", "descripcion": "Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio.", "stock": 92, "imagen": "http://dummyimage.com/160x100.png/cc0000/ffffff", "categoria": "Honda" },
    { "id": 16, "precio": 795, "titulo": "9-3", "descripcion": "Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.", "stock": 30, "imagen": "http://dummyimage.com/206x100.png/ff4444/ffffff", "categoria": "Saab" },
    { "id": 17, "precio": 58, "titulo": "Panamera", "descripcion": "Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.", "stock": 71, "imagen": "http://dummyimage.com/194x100.png/dddddd/000000", "categoria": "Porsche" },
    { "id": 18, "precio": 743, "titulo": "850", "descripcion": "Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam. Nam tristique tortor eu pede.", "stock": 39, "imagen": "http://dummyimage.com/109x100.png/cc0000/ffffff", "categoria": "Volvo" },
    { "id": 19, "precio": 125, "titulo": "Odyssey", "descripcion": "In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.", "stock": 35, "imagen": "http://dummyimage.com/127x100.png/ff4444/ffffff", "categoria": "Honda" },
    { "id": 20, "precio": 60, "titulo": "Sunbird", "descripcion": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.", "stock": 84, "imagen": "http://dummyimage.com/112x100.png/5fa2dd/ffffff", "categoria": "Pontiac" },
    { "id": 21, "precio": 654, "titulo": "Explorer Sport", "descripcion": "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.", "stock": 90, "imagen": "http://dummyimage.com/134x100.png/5fa2dd/ffffff", "categoria": "Ford" },
    { "id": 22, "precio": 317, "titulo": "Maxima", "descripcion": "Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.", "stock": 52, "imagen": "http://dummyimage.com/157x100.png/cc0000/ffffff", "categoria": "Nissan" },
    { "id": 23, "precio": 429, "titulo": "5 Series", "descripcion": "Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.", "stock": 42, "imagen": "http://dummyimage.com/250x100.png/dddddd/000000", "categoria": "BMW" },
    { "id": 24, "precio": 726, "titulo": "Jetta", "descripcion": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.", "stock": 99, "imagen": "http://dummyimage.com/113x100.png/5fa2dd/ffffff", "categoria": "Volkswagen" },
    { "id": 25, "precio": 643, "titulo": "PT Cruiser", "descripcion": "In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.", "stock": 44, "imagen": "http://dummyimage.com/153x100.png/dddddd/000000", "categoria": "Chrysler" },
    { "id": 26, "precio": 246, "titulo": "500SL", "descripcion": "Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.", "stock": 99, "imagen": "http://dummyimage.com/187x100.png/dddddd/000000", "categoria": "Mercedes-Benz" },
    { "id": 27, "precio": 848, "titulo": "Aerio", "descripcion": "Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.", "stock": 15, "imagen": "http://dummyimage.com/237x100.png/cc0000/ffffff", "categoria": "Suzuki" },
    { "id": 28, "precio": 302, "titulo": "929", "descripcion": "Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.", "stock": 59, "imagen": "http://dummyimage.com/136x100.png/cc0000/ffffff", "categoria": "Mazda" },
    { "id": 29, "precio": 793, "titulo": "4Runner", "descripcion": "Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.", "stock": 69, "imagen": "http://dummyimage.com/249x100.png/cc0000/ffffff", "categoria": "Toyota" },
    { "id": 30, "precio": 745, "titulo": "DeVille", "descripcion": "Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.", "stock": 42, "imagen": "http://dummyimage.com/223x100.png/5fa2dd/ffffff", "categoria": "Cadillac" }]

    export const dbPedidos = [
        {
          "id": 1,
          "productos": [
            { 
              "id": 1,
              "precio": 883,
              "titulo": "XK",
              "descripcion": "Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla.",
              "stock": 95,
              "imagen": "http://dummyimage.com/112x100.png/cc0000/ffffff",
              "categoria": "Jaguar",
              "cantidad": 2,
              "subtotal": 1766
            },
            { 
              "id": 3,
              "precio": 172,
              "titulo": "RSX",
              "descripcion": "In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.",
              "stock": 48,
              "imagen": "http://dummyimage.com/146x100.png/cc0000/ffffff",
              "categoria": "Acura",
              "cantidad": 1,
              "subtotal": 172
            }
          ],
          "estado": "pendiente",
          "datosEnvio": {
            "nombre": "Juan Pérez",
            "direccion": "Calle 123, Ciudad",
            "telefono": "123456789"
          },
          "datosPago": {
            "tarjeta": "**** **** **** 1234",
            "subtotal": 1938,
            "cupon": "DESCUENTO20",
            "iva": 0.16,
            "montoTotal": 2038
          }
        },
        {
          "id": 2,
          "productos": [
            { 
              "id": 2,
              "precio": 722,
              "titulo": "Prelude",
              "descripcion": "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.",
              "stock": 31,
              "imagen": "http://dummyimage.com/166x100.png/ff4444/ffffff",
              "categoria": "Honda",
              "cantidad": 1,
              "subtotal": 722
            },
            { 
              "id": 5,
              "precio": 872,
              "titulo": "Tracer",
              "descripcion": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.",
              "stock": 97,
              "imagen": "http://dummyimage.com/127x100.png/dddddd/000000",
              "categoria": "Mercury",
              "cantidad": 3,
              "subtotal": 2616
            }
          ],
          "estado": "en preparacion",
          "datosEnvio": {
            "nombre": "Ana García",
            "direccion": "Avenida XYZ, Ciudad",
            "telefono": "987654321"
          },
          "datosPago": {
            "tarjeta": "**** **** **** 5678",
            "subtotal": 3338,
            "cupon": null,
            "iva": 0.12,
            "montoTotal": 3728
          }
        },
      ];
      