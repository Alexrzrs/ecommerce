import React, { useState } from "react";
import { Input, Space } from "antd";
import { SearchOutlined } from "@ant-design/icons";

const { Search } = Input;

const Buscador = ({ onSearch }) => {
    const [searchText, setSearchText] = useState("");

    const handleSearch = () => {
        onSearch(searchText);
    };

    return (
        <Space style={{ marginBottom: "23px" }}>
            <Search
                placeholder="Buscar productos"
                allowClear
                enterButton={<SearchOutlined />}
                size="middle"
                onSearch={handleSearch}
                onChange={(e) => setSearchText(e.target.value)}
                value={searchText}
                style={{
                    width: "300px", // ajusta el ancho según tus preferencias
                    boxShadow:
                        "0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.05)",
                    borderRadius: "20px",
                }}
            />
        </Space>
    );
};

export default Buscador;
