import { Card, Carousel, Col, Row, Typography } from "antd";
import React, { useEffect, useState } from "react";
import "./Recomendaciones.css";
import {
    collection,
    getDocs,
    query,
    orderBy,
    limit,
} from "@firebase/firestore";
import { db } from "../firebaseConfig";
import { Link } from "react-router-dom";

export const Recomendaciones = () => {
    const [recomendaciones, setRecomendaciones] = useState([]);

    const preloadImage = (url) => {
        const link = document.createElement("link");
        link.rel = "preload";
        link.as = "image";
        link.href = url;
        document.head.appendChild(link);
    };

    useEffect(() => {
        const getRecomendaciones = async () => {
            let recs = [];
            const q = query(
                collection(db, "productos"),
                orderBy("stock"),
                limit(5)
            );
            const querySnapshot = await getDocs(q);
            querySnapshot.forEach((doc) => {
                const producto = doc.data();
                producto.id = doc.id;
                recs.push(producto);
            });
            setRecomendaciones(recs);

            // Preload images after fetching
            recs.forEach((producto) => {
                preloadImage(producto.imagenes[0]);
            });
        };

        getRecomendaciones();
    }, []);

    return (
        <div className="recomendaciones-container">
            {recomendaciones != null ? (
                <>
                    <Typography.Title>
                        ¡Llevatelos porque vuelan!
                    </Typography.Title>
                    <Carousel>
                        {recomendaciones.map((producto) => (
                            <Card key={producto.id}>
                                <Row>
                                    <Col xs={24} md={12}>
                                        <img
                                            src={producto.imagenes[0]}
                                            alt="imagen"
                                            className="recomendacion-img"
                                        />
                                    </Col>
                                    <Col xs={24} md={12}>
                                        <Link to={`/producto/${producto.id}`}>
                                            <Typography.Title level={2}>
                                                {producto.nombre}
                                            </Typography.Title>
                                        </Link>
                                        <Typography.Title level={3}>
                                            ${producto.precio}
                                        </Typography.Title>
                                        <Typography.Paragraph
                                            ellipsis={{ rows: 2 }}
                                        >
                                            {producto.descripcion}
                                        </Typography.Paragraph>
                                    </Col>
                                </Row>
                            </Card>
                        ))}
                    </Carousel>
                </>
            ) : null}
        </div>
    );
};
