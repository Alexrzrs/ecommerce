import React, { useState } from "react";
import { Card, Button, Input, Select, Collapse } from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons"; // Importa el icono QuestionCircleOutlined
import "./ChatBubble.css";

const { Option } = Select;
const { Panel } = Collapse;

const ChatBubble = () => {
    const [message, setMessage] = useState("");
    const [selectedOption, setSelectedOption] = useState("");
    const [isCollapsed, setIsCollapsed] = useState(false);

    const handleSendMessage = () => {
        console.log(
            "Mensaje enviado:",
            message,
            "Opción seleccionada:",
            selectedOption
        );
    };

    const toggleCollapse = () => {
        setIsCollapsed(!isCollapsed);
    };

    return (
        <div className="chat-bubble">
            <Collapse defaultActiveKey={["0"]} expandIconPosition="end">
                <Panel
                    key="1"
                    style={{
                        width: "100%",
                        background: "#fff",
                        color: "black",
                        borderRadius: 20,
                    }}
                    extra={
                        isCollapsed ? (
                            <QuestionCircleOutlined
                                style={{ fontSize: 16 }}
                                onClick={toggleCollapse}
                            />
                        ) : (
                            <QuestionCircleOutlined
                                style={{ fontSize: 16 }}
                                onClick={toggleCollapse}
                            />
                        )
                    }
                >
                    <Card
                        title="Asistente Virtual"
                        style={{
                            width: "100%",
                            background: "#fff",
                            color: "black",
                        }}
                    >
                        Hola, ¿en qué puedo ayudarte?
                    </Card>
                    <Input
                        placeholder="Escribe tu mensaje"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        style={{ marginBottom: 16 }}
                    />
                    <Select
                        placeholder="Selecciona una opción"
                        style={{ width: "100%", marginBottom: 16 }}
                        value={selectedOption}
                        onChange={(value) => setSelectedOption(value)}
                    >
                        <Option value="atencion_cliente">
                            Atención al Cliente
                        </Option>
                        <Option value="enviar_correo">Enviar Correo</Option>
                    </Select>
                    <Button type="primary" onClick={handleSendMessage}>
                        Enviar
                    </Button>
                </Panel>
            </Collapse>
        </div>
    );
};

export default ChatBubble;
