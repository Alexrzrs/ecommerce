import React, { useState } from "react";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
import "./Registro.css";
import { auth, createUserWithEmailAndPassword } from "../firebaseConfig";
import { getFirestore, collection, doc, setDoc } from "firebase/firestore";
import { useNavigate } from "react-router-dom";
import { Toaster, toast } from "sonner";

const Registro = () => {
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const onFinish = async (values) => {
        setLoading(true);

        try {
            const userCredential = await createUserWithEmailAndPassword(
                auth,
                values.email,
                values.password
            );
            setLoading(false);
            const userId = userCredential.user.uid;

            // Obtener una instancia de Firestore
            const db = getFirestore();

            const usersCollection = collection(db, "roles");
            const userDocRef = doc(usersCollection, userId);
            await setDoc(userDocRef, {
                username: values.username,
                email: values.email,
                role: 0,
            });

            // console.log("Usuario registrado:", userCredential.user);
            toast.success("Registro exitoso", {
                duration: 5000,
                action: {
                    label: "Aceptar",
                    onClick: () => navigate("/login"),
                },
            });
        } catch (error) {
            setLoading(false);
            toast.error("Error en el registro. Por favor, inténtalo de nuevo.");
            console.error(error.message);
        }
    };
    return (
        <div className="registration-container">
            <Toaster richColors position="top-center" />
            <Form
                name="register"
                onFinish={onFinish}
                scrollToFirstError
                className="registration-form"
            >
                <h1 className="registration-title">Registro de Usuario</h1>
                <Form.Item
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: "Por favor, ingresa tu nombre de usuario",
                        },
                    ]}
                >
                    <Input
                        prefix={
                            <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Nombre de Usuario"
                    />
                </Form.Item>

                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message:
                                "Por favor, ingresa tu dirección de correo electrónico",
                        },
                        {
                            type: "email",
                            message: "Ingresa un correo electrónico válido",
                        },
                    ]}
                >
                    <Input
                        prefix={
                            <MailOutlined className="site-form-item-icon" />
                        }
                        placeholder="Correo Electrónico"
                    />
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Por favor, ingresa tu contraseña",
                        },
                        {
                            min: 8,
                            message:
                                "La contraseña debe tener al menos 8 caracteres",
                        },
                    ]}
                >
                    <Input.Password
                        prefix={
                            <LockOutlined className="site-form-item-icon" />
                        }
                        placeholder="Contraseña"
                    />
                </Form.Item>

                <Form.Item
                    name="confirm"
                    dependencies={["password"]}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: "Por favor, confirma tu contraseña",
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (
                                    !value ||
                                    getFieldValue("password") === value
                                ) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(
                                    "Las contraseñas no coinciden"
                                );
                            },
                        }),
                    ]}
                >
                    <Input.Password
                        prefix={
                            <LockOutlined className="site-form-item-icon" />
                        }
                        placeholder="Confirmar Contraseña"
                    />
                </Form.Item>

                <Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        loading={loading}
                        className="registration-form-button"
                    >
                        Registrarse
                    </Button>
                </Form.Item>

                <Form.Item style={{ textAlign: "center" }}>
                    ¿Ya tienes una cuenta? <a href="/login">Inicia Sesión</a>
                </Form.Item>
            </Form>
        </div>
    );
};

export default Registro;
