import React, { useState, useEffect } from "react";
import { doc, onSnapshot, collection, updateDoc } from "firebase/firestore";
import { db } from "../firebaseConfig";
import { Button, Table, Modal as AntModal, Select } from "antd";
import {
    ClockCircleOutlined,
    LoadingOutlined,
    CheckCircleOutlined,
} from '@ant-design/icons';
import './GestionPedidos.css'

const { Option } = Select;

const GestionPedidos = () => {
    const [orders, setOrders] = useState([]);
    const [selectedOrder, setSelectedOrder] = useState(null);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [newStatus, setNewStatus] = useState('');

    useEffect(() => {
        const ordersCollection = collection(db, "pedidos");

        const unsubscribe = onSnapshot(ordersCollection, (snapshot) => {
            const newOrders = snapshot.docs.map((doc) => ({
                key: doc.id,
                id: doc.id,
                ...doc.data(),
            }));
            setOrders(newOrders);
        });

        return () => unsubscribe();
    }, []);

    const getStatusTag = (estado) => {
        let color, icon;

        switch (estado) {
            case "pendiente":
                color = "orange";
                icon = <ClockCircleOutlined />;
                break;
            case "en preparacion":
                color = "blue";
                icon = <LoadingOutlined />;
                break;
            case "enviado":
                color = "green";
                icon = <CheckCircleOutlined />;
                break;
            default:
                color = "default";
                icon = null;
                break;
        }

        return (
            <span style={{ color, marginRight: '8px' }}>
                {icon} {estado}
            </span>
        );
    };

    const updateOrderStatus = async (orderId) => {
        try {
            const orderRef = doc(db, "pedidos", orderId);
            await updateDoc(orderRef, { estado: newStatus });

            console.log(`Estado actualizado para el pedido ${orderId} a ${newStatus}`);
        } catch (error) {
            console.error("Error al actualizar el estado del pedido:", error.message);
        }
    };

    const openModal = (order) => {
        setSelectedOrder(order);
        setModalIsOpen(true);
    };

    const closeModal = () => {
        setSelectedOrder(null);
        setModalIsOpen(false);
    };

    const columns = [
        {
            title: 'Order ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Estado',
            dataIndex: 'estado',
            key: 'estado',
            render: (estado) => (
                <>
                 {getStatusTag(estado)}
                </>
            ),
        },
        {
            title: 'Acciones',
            dataIndex: 'acciones',
            key: 'acciones',
            render: (_, record) => (
                <>
                    <Select
                        className="status-select"
                        defaultValue={record.estado}
                        onChange={(value) => setNewStatus(value)}
                    >
                        <Option value="pendiente">Pendiente</Option>
                        <Option value="en preparacion">En Preparación</Option>
                        <Option value="enviado">Enviado</Option>
                    </Select>
                    <Button
                        className="update-status-button"
                        onClick={() => updateOrderStatus(record.id)}
                    >
                        Actualizar Estado
                    </Button>
                    <Button
                        className="details-button"
                        onClick={() => openModal(record)}
                    >
                        Ver Detalles
                    </Button>
                </>
            ),
        },
    ];

    return (
        <div className="gestion-pedidos-container"> 
        <h1 className="pedidos-heading">Lista de Pedidos</h1> 
        <div className="table-container">
            <Table
                dataSource={orders}
                columns={columns}
                pagination={false} 
            />
        </div>

        <AntModal
            title="Detalles del Pedido"
            visible={modalIsOpen}
            onCancel={closeModal}
            footer={[
                <Button
                    key="cerrar"
                    type="primary"
                    onClick={closeModal}
                >
                    Cerrar
                </Button>,
            ]}
        >
                {selectedOrder && (
                    <div>
                       <h3>Productos</h3>
                        <ul>
                            {selectedOrder.productos.map((producto, index) => (
                                <li key={index}>
                                    <h4>{producto.titulo}</h4>
                                    <p>Precio: ${producto.precio}</p>
                                    <p>Cantidad: {producto.cantidad}</p>
                                    <p>Subtotal: ${producto.cantidad * producto.precio}</p>
                                </li>
                            ))}
                        </ul>
                        <h3>Datos de Envío</h3>
                        <p>Nombre: {selectedOrder.nombre}</p>
                        <p>Dirección: {selectedOrder.direccion}</p>
                        <p>Teléfono: {selectedOrder.telefono}</p>
                        <h3>Datos de Pago</h3>
                        <p>Tarjeta: {selectedOrder.numeroTarjeta}</p>
                        <p>Subtotal: ${selectedOrder.subtotal}</p>
                        <p>Cupón: {selectedOrder.cupon}</p>
                        <p>Monto Total: ${selectedOrder.total}</p>
                    </div>
                )}
            </AntModal>
        </div>
    );
};

export default GestionPedidos;



