import React from "react";
import { Card, Space, Typography, Button, InputNumber } from "antd";
import { doc, updateDoc, getDoc } from "firebase/firestore";
import { db } from "../firebaseConfig";
import { useUser } from "../context/UserContext";

const { Text } = Typography;

const CartItem = ({ item }) => {
    const { userData } = useUser();

    const updateCartQuantity = async (productId, newQuantity) => {
        try {
            const userCartRef = doc(db, "carritos", userData.uid);
            const userCartDoc = await getDoc(userCartRef);

            if (userCartDoc.exists()) {
                const updatedProducts = userCartDoc
                    .data()
                    .productos.map((product) => {
                        if (product.id === productId) {
                            return {
                                ...product,
                                cantidad: newQuantity,
                            };
                        }
                        return product;
                    });

                await updateDoc(userCartRef, { productos: updatedProducts });
                // console.log('Cantidad actualizada en el carrito.');
            } else {
                console.error("El carrito no existe.");
            }
        } catch (error) {
            console.error(
                "Error al actualizar la cantidad en el carrito:",
                error.message
            );
        }
    };

    const handleQuantityChange = async (newQuantity) => {
        updateCartQuantity(item.id, newQuantity);
    };

    const handleRemoveFromCart = async () => {
        try {
            const userCartRef = doc(db, "carritos", userData.uid);
            const userCartDoc = await getDoc(userCartRef);

            if (userCartDoc.exists()) {
                const updatedProducts = userCartDoc
                    .data()
                    .productos.filter((product) => product.id !== item.id);

                await updateDoc(userCartRef, { productos: updatedProducts });
                // console.log('Producto eliminado del carrito.');
            } else {
                console.error("El carrito no existe.");
            }
        } catch (error) {
            console.error(
                "Error al eliminar el producto del carrito:",
                error.message
            );
        }
    };

    return (
        <Card style={{ marginBottom: "16px" }}>
            <Space direction="vertical" size="small">
                <h3>{item.nombre}</h3>
                <Text>Precio: ${item.precio}</Text>
                <Text>
                    Cantidad:
                    <InputNumber
                        min={1}
                        max={10}
                        value={item.cantidad}
                        onChange={handleQuantityChange}
                    />
                </Text>
                <Text type="secondary">
                    Subtotal: ${item.precio * item.cantidad}
                </Text>
                <Button type="danger" onClick={handleRemoveFromCart}>
                    Eliminar del carrito
                </Button>
            </Space>
        </Card>
    );
};

export default CartItem;
