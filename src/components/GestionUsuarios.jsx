import React, { useEffect, useState } from "react";
import { List, Select, Button} from "antd";
import { db } from "../firebaseConfig"
import "./GestionUsuarios.css";
import {
    collection,
    getDocs, 
    deleteField,
    deleteDoc,
    onSnapshot,
    doc
} from "firebase/firestore";

const GestionUsuarios = () => {
    const [users, setUsers] = useState([]);

  useEffect(() => {
    const unsubscribe = onSnapshot(collection(db, "roles"), (querySnapshot) => {
      const updatedUsers = [];
      querySnapshot.forEach((doc) => {
        const user = doc.data();
        user.id = doc.id;
        if (user.email != null && user.role === 0) {
          updatedUsers.push(user);
        }
      });
      setUsers(updatedUsers);
    });

    return () => {
      // Esta función de limpieza se ejecutará al desmontar el componente
      unsubscribe();
    };
  }, []); // El array vacío asegura que este efecto se ejecute solo una vez al montar el componente

  const deleteUser = async (userId) => {
    try {
      const userDocRef = doc(collection(db, "roles"), userId);
      await deleteDoc(userDocRef);

      console.log('Usuario eliminado con éxito');
    } catch (error) {
      console.error('Error al eliminar usuario:', error);
    }
  };

    return (
        <div className="usuarios-container">
            <h3 style={{ marginBottom: "20px" }}>Lista de Usuarios</h3>
            <List
                itemLayout="horizontal"
                dataSource={users}
                renderItem={(user) => (
                    <List.Item
                        actions={[
                            <Button
                                className="delete-button"
                                type="primary"
                                onClick={() => deleteUser(user.id)}
                            >
                                Eliminar
                            </Button>,
                        ]}
                    >
                        <List.Item.Meta
                            title={`Usuario: ${user.username}`}
                            description={`Email: ${user.email}`}
                        />
                    </List.Item>
                )}
            />
        </div>
    );
};

export default GestionUsuarios;
