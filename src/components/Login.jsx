import React, { useState, useEffect } from "react";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import "./Login.css";
import { auth, signInWithEmailAndPassword } from "../firebaseConfig";
import { getFirestore, doc, getDoc } from "firebase/firestore";
import { useNavigate } from "react-router-dom";
import { Toaster, toast } from "sonner";
import { useUser } from "../context/UserContext";
const Login = () => {
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();
    const { setUser, userData } = useUser();

    useEffect(() => {
        // console.log('userData after setUser:', userData);
    }, [userData]);

    const onFinish = async (values) => {
        setLoading(true);
        try {
            const userCredential = await signInWithEmailAndPassword(
                auth,
                values.email,
                values.password
            );

            // console.log(userCredential)

            // Obtener una instancia de Firestore
            const db = getFirestore();
            const userId = userCredential.user.uid;

            // Obtener el documento de roles del usuario
            const userDoc = await getDoc(doc(db, "roles", userId));
            const userData = userDoc.data();

            // Obtener más datos del usuario según tus necesidades
            const datosUser = {
                uid: userId,
                email: userCredential.user.email,
                session: true,
                // Otros datos que desees incluir
            };
            // console.log(datosUser);

            // Guardar todos los datos del usuario en el contexto global

            setUser(datosUser);

            setLoading(false);

            if (userData && userData.role === 1) {
                toast.success("Inicio de sesión exitoso", {
                    duration: 5000,
                    action: {
                        label: "Aceptar",
                        onClick: () => navigate("/gestion_pedidos"),
                    },
                });
            } else {
                toast.success("Inicio de sesión exitoso", {
                    duration: 5000,
                    action: {
                        label: "Aceptar",
                        onClick: () => navigate("/"),
                    },
                });
            }
        } catch (error) {
            setLoading(false);
            toast.error(
                "Credenciales incorrectas. Por favor, inténtalo de nuevo."
            );
            console.error(error.message);
        }
    };
    return (
        <div className="login-container">
            <Toaster richColors position="top-center" />
            <Form
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                className="login-form"
            >
                <h1 className="login-title">Inicio de Sesión</h1>
                <Form.Item
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: "Por favor, ingresa tu correo",
                        },
                    ]}
                >
                    <Input
                        prefix={
                            <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Correo electrónico"
                    />
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Por favor, ingresa tu contraseña",
                        },
                    ]}
                >
                    <Input.Password
                        prefix={
                            <LockOutlined className="site-form-item-icon" />
                        }
                        placeholder="Contraseña"
                    />
                </Form.Item>

                <Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        loading={loading}
                        className="login-form-button"
                    >
                        Iniciar Sesión
                    </Button>
                </Form.Item>

                <Form.Item>
                    ¿No tienes una cuenta?{" "}
                    <a href="/registro">Regístrate aquí</a>
                </Form.Item>
            </Form>
        </div>
    );
};

export default Login;
