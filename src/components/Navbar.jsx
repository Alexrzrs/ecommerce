import React, { useEffect, useState } from "react";
import BtnCarrito from "./BtnCarrito";
import { useUser } from "../context/UserContext";
import { doc, onSnapshot } from "firebase/firestore";
import { auth, db } from "../firebaseConfig";
import logo from "../Assets/logo.jpeg";
import { Button } from "antd";
import { LogoutOutlined, SearchOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router";

const Navbar = () => {
    const { setUser, userData } = useUser();
    const [cartItems, setCartItems] = useState([]);
    const navigate = useNavigate()

    useEffect(() => {
        let unsubscribe; // Variable para almacenar la función de desinscripción

        const fetchCartItems = async () => {
            try {
                if (userData) {
                    // Obtener el carrito del usuario desde la base de datos
                    const userCartRef = doc(db, "carritos", userData.uid);

                    // Establecer un listener para cambios en tiempo real
                    unsubscribe = onSnapshot(userCartRef, (doc) => {
                        if (doc.exists()) {
                            // Si el carrito existe, actualiza el estado con los productos del carrito
                            setCartItems(doc.data().productos);
                        } else {
                            // Si el carrito no existe, establece el estado como un arreglo vacío
                            setCartItems([]);
                        }
                    });
                } else {
                    // Si el usuario no está autenticado, establece el estado como un arreglo vacío
                    setCartItems([]);
                }
            } catch (error) {
                console.error(
                    "Error al obtener productos del carrito:",
                    error.message
                );
            }
        };

        // Llama a la función para obtener los productos del carrito
        fetchCartItems();

        // Retorna la función de desinscripción para limpiar cuando el componente se desmonta
        return () => {
            if (unsubscribe) {
                unsubscribe();
            }
        };
    }, [userData]);

    const logout = async () => {
        auth.signOut().then(() => {
            setUser(null)
            navigate("/")
        }).catch(error => {
            console.log(error)
        })
    }

    return (
        <div
            style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                padding: "10px",
                borderBottom: "1px solid #ccc",
                width: "100%",
                backgroundColor: "#d4a373",
            }}
        >
            <div
                style={{
                    margin: "15px",
                }}
            >
                <img style={{ height: 50, width: 150 }} src={logo} alt="Logo" />
            </div>
            <div style={{ margin: "15px", display: 'flex', gap: '10px' }}>
                {userData && <Button type="primary" onClick={logout} icon={<LogoutOutlined />}>Cerrar sesión</Button>}
                {/* Pasa los productos del carrito al componente BtnCarrito */}
                <BtnCarrito cartItems={cartItems} />
            </div>
        </div>
    );
};

export default Navbar;
