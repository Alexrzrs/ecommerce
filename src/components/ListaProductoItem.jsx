import { ShoppingCartOutlined } from "@ant-design/icons";
import { Card, Typography } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import "./ListaProductoItem.css";

const { Meta } = Card;

export const ListaProductoItem = ({ producto, addToCart }) => {
    return (
        <Card
            style={{ width: 300, borderRadius: 10, borderColor: "black" }}
            cover={
                <Link key={producto.id} to={`producto/${producto.id}`}>
                    <img
                        alt="example"
                        src={producto.imagenes[0]}
                        style={{
                            width: 300,
                            height: 100,
                            objectFit: "contain",
                            padding: 2,
                        }}
                    />
                </Link>
            }
            bodyStyle={{ backgroundColor: "#6EDCFA" }}
            actions={[
                <Typography.Text
                    style={{
                        color: "#00aae7",
                        fontWeight: "bolder",
                        backgroundColor: "#ffedd8",
                    }}
                >
                    ${producto.precio}
                </Typography.Text>,
                <button
                    onClick={() => addToCart(producto)}
                    style={{
                        color: "green",
                        background: "#ffedd8",
                        border: "none",
                        cursor: "pointer",
                        fontWeight: "bolder",
                    }}
                >
                    <ShoppingCartOutlined style={{ color: "green" }} />
                    Agregar al carrito
                </button>,
            ]}
        >
            <Meta
                title={producto.nombre}
                description={
                    <div
                        style={{ color: "midnightblue" }}
                    >{`${producto.stock} en stock`}</div>
                }
            />
        </Card>
    );
};
