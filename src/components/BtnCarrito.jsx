import React from "react";
import { Badge, Button, Drawer } from "antd";
import CartView from "../screens/CartView";
import { ShoppingCartOutlined } from "@ant-design/icons";

const BtnCarrito = ({ cartItems }) => {
    const [drawerVisible, setDrawerVisible] = React.useState(false);

    const showDrawer = () => {
        setDrawerVisible(true);
    };

    const onClose = () => {
        setDrawerVisible(false);
    };

    return (
        <>
            <Badge count={cartItems.length}>
                <Button icon={<ShoppingCartOutlined />} onClick={showDrawer} />
            </Badge>
            <Drawer
                title="Carrito de Compras"
                placement="right"
                closable={false}
                onClose={onClose}
                open={drawerVisible}
            >
                <CartView cartItems={cartItems} />
            </Drawer>
        </>
    );
};

export default BtnCarrito;
