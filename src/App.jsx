import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Registro from "./components/Registro";
import Login from "./components/Login";
import Productos from "./screens/Productos";
import Producto from "./screens/Producto";
import Carrito from "./screens/CartView";
import Pedidos from "./screens/Pedidos";
import Gestion from "./screens/Gestion";
import { UserProvider } from "./context/UserContext";

export default function App() {
    return (
        <BrowserRouter>
            <UserProvider>
                <Routes>
                    <Route path="" element={<Productos />} />
                    <Route path="/producto/:id" element={<Producto />} />
                    <Route path="/registro" element={<Registro />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/carrito" element={<Carrito />} />
                    <Route path="/pedidos" element={<Pedidos />} />
                    <Route
                        path="/gestion"
                        element={<Gestion />}
                    />
                </Routes>
            </UserProvider>
        </BrowserRouter>
    );
}
