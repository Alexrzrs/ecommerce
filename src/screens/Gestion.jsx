import React, { useState } from "react";
import { Modal, List, Select, Button, Tag } from "antd";
import GestionPedidos from "../components/GestionPedidos";
import GestionUsuarios from "../components/GestionUsuarios";
import "./Gestion.css";

const Gestion= () => {
    const [pestañaActiva, setPestañaActiva] = useState('pedidos');

    const cambiarPestaña = (nuevaPestaña) => {
        setPestañaActiva(nuevaPestaña);
    };
    return (
        <div className="gestion-container">
            <h1 style={{ marginBottom: "20px" }}>Panel administartivo</h1>
            <div className="gestion-buttons">
                <Button className="gestion-button" onClick={() => cambiarPestaña('pedidos')}>Pedidos</Button>
                <Button className="gestion-button" onClick={() => cambiarPestaña('usuarios')}>Usuarios</Button>
            </div>

            <div>
                {pestañaActiva === 'pedidos' && <GestionPedidos />}
                {pestañaActiva === 'usuarios' && <GestionUsuarios />}
            </div>
    </div>
    );
};

export default Gestion;
