import React, { useState } from "react";
import CartItem from "../components/CartItem";
import { Button, Empty, Typography } from "antd";
import CuponModal from "./CuponModal";
import { Toaster, toast } from "sonner";
import { TagOutlined } from "@ant-design/icons";
import "./CartView.css";
import DireccionPagoModal from "./DireccionPagoModal";

const { Text } = Typography;

const CartView = ({ cartItems }) => {
  const [appliedCoupon, setAppliedCoupon] = useState("");
  const [discount, setDiscount] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [modalActive, setModalActive] = useState(false);
  const [couponApplied, setCouponApplied] = useState(false);

  const handleApplyCoupon = (couponCode) => {
    if (couponApplied) {
      toast.warning("El cupón ya ha sido aplicado");
      return;
    }

    if (couponCode === "DISCOUNT10") {
      setAppliedCoupon(couponCode);
      setDiscount(0.1); // 10% de descuento

      setModalVisible(false);
      toast.success("¡Cupón aplicado correctamente!", {
        duration: 5000,
      });
      setCouponApplied(true);
    } else {
      setAppliedCoupon("");
      setDiscount(0);
      toast.error("El cupón no es válido");
    }
  };

  const onCreate = (values) => {
    console.log("Received values of form: ", values);
    setModalActive(false);
  };

  const getTotalPrice = (cartItems) => {
    return cartItems.reduce((total, item) => {
      const itemPrice = typeof item.precio === "number" ? item.precio : 0;
      const itemQuantity =
        typeof item.cantidad === "number" ? item.cantidad : 0;
      return total + itemPrice * itemQuantity;
    }, 0);
  };

  const total =
    Array.isArray(cartItems) && cartItems.length > 0
      ? getTotalPrice(cartItems)
      : 0;
  const discountedTotal = total - total * discount;
  console.log(cartItems);
  return (
    <div style={{ padding: "20px" }}>
      <Toaster richColors position="top-center" />
      <h2 style={{ marginBottom: "20px" }}>Carrito de Compras</h2>
      {cartItems && cartItems.length > 0 ? (
        <>
          {cartItems.map((item) => (
            <CartItem key={item.id} item={item} />
          ))}
          <div style={{ marginTop: "20px", textAlign: "right" }}>
            <Button
              type="primary"
              className="ButtonDesc"
              onClick={() => setModalVisible(true)}
              icon={<TagOutlined />}
              disabled={couponApplied} // Desactiva el botón si el cupón ya se ha aplicado
            >
              Ingresar Cupón
            </Button>
            <CuponModal
              visible={modalVisible}
              onCancel={() => setModalVisible(false)}
              onApply={handleApplyCoupon}
            />

            <Text
              type="danger"
              strong
              style={{ display: "block", marginTop: "10px" }}
            >
              Total: ${discountedTotal.toFixed(2)}
              {appliedCoupon && <span> (Cupón aplicado: {appliedCoupon})</span>}
            </Text>
            <Button
              type="primary"
              style={{ marginLeft: "10px" }}
              onClick={() => setModalActive(true)}
            >
              Completar Compra
            </Button>
            <DireccionPagoModal
              visible={modalActive}
              onCancel={() => setModalActive(false)}
              onCreate={onCreate}
              cartItems={cartItems}
              appliedCoupon={appliedCoupon}
            />
          </div>
        </>
      ) : (
        <Empty description="No hay productos en el carrito" />
      )}
    </div>
  );
};

export default CartView;
