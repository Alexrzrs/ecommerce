import React, { useState } from "react";
import { Modal, Form, Input, Select, Radio } from "antd";
import { addDoc, collection, doc ,  deleteDoc  } from "firebase/firestore";
import { db } from "../firebaseConfig"; // Asegúrate de importar tu configuración de Firebase
import { useUser } from "../context/UserContext";

const DireccionPagoModal = ({ visible, onCancel, onCreate, cartItems, appliedCoupon }) => {
    const [form] = Form.useForm();
    const { Option } = Select;
    const { userData } = useUser();

    const userCartRef = doc(db, "carritos", userData.uid);

    const handleFormSubmit = async () => {
        try {
            const values = await form.validateFields();
            form.resetFields();
    
            // Verificar si se aplicó el cupón DISCOUNT10
            let discount = 0;
            if (appliedCoupon === "DISCOUNT10") {
                discount = 0.1; // 10% de descuento
            }
    
            // Calcular el subtotal
            const subtotal = getTotalPrice(cartItems);
    
            // Calcular el total con descuento
            const total = subtotal - subtotal * discount;
    
            // Crear un nuevo documento en la colección "pedidos" en Firestore
            const pedidosCollection = collection(db, "pedidos");
            const nuevoPedido = {
                idUsuario: userData.uid, // Asegúrate de obtener el id del usuario
                productos: cartItems,
                cupon: appliedCoupon,
                subtotal: subtotal,
                total: total,
                nombre: values.name,
                direccion: values.address,
                codigoPostal: values.postalCode,
                telefono: values.phone,
                numeroTarjeta: values.numberCard,
                fechaVencimiento: `${values.monthCard}/${values.ageCard}`,
                ccv: values.ccv,
                tipoEnvio: values.typeSend,
                estado: "pendiente"
                // Otros campos que desees almacenar en el pedido...
            };
            await addDoc(pedidosCollection, nuevoPedido);
            borrarCarrito();
            // Llamar a la función onCreate
            onCreate(values);
        } catch (errorInfo) {
            console.log("Failed:", errorInfo);
        }
    };
    
    
    const getTotalPrice = (cartItems) => {
        return cartItems.reduce((total, item) => {
            const itemPrice = typeof item.precio === "number" ? item.precio : 0;
            const itemQuantity = typeof item.cantidad === "number" ? item.cantidad : 0;
            return total + itemPrice * itemQuantity;
        }, 0);
    };
    
    const borrarCarrito = async () => {
        try {
            await deleteDoc(userCartRef);
            console.log("Carrito borrado exitosamente.");
        } catch (error) {
            console.error("Error al intentar borrar el carrito:", error);
        }
    };
    return (
        <Modal
            title="Dirección de Envío y Método de Pago"
            visible={visible}
            onCancel={onCancel}
            okText="Finalizar Compra"
            cancelText="Cancelar"
            onOk={handleFormSubmit}
        >
       
            <Form
                form={form}
                layout="vertical"
                name="addresPaid"
                initialValues={{
                    modifier: "public",
                }}
            >
                <Form.Item
                    name="name"
                    label="Nombre completo (nombre y apellidos)"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                    ]}
                >
                    <Input placeholder="Nombre, apellido paterno, apellido Materno" />
                </Form.Item>
                <Form.Item
                    name="address"
                    label="Calle y número"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                    ]}
                >
                    <Input placeholder="Calle, número ext e int" />
                </Form.Item>
                <Form.Item
                    name="postalCode"
                    label="Código Postal"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                        () => ({
                            validator(_, value) {
                                if (
                                    !value ||
                                    (value.length === 5 && /^\d+$/.test(value))
                                ) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(
                                    new Error("Se deben ingresar 5 números")
                                );
                            },
                        }),
                    ]}
                >
                    <Input
                        placeholder="Por ejemplo, 01000"
                        maxLength={5}
                        type="text"
                        pattern="\d*"
                        title="Only numbers are allowed"
                        style={{ width: "40%" }}
                    />
                </Form.Item>

                <Form.Item
                    name="phone"
                    label="Número de teléfono"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                        () => ({
                            validator(_, value) {
                                if (
                                    !value ||
                                    (value.length === 10 && /^\d+$/.test(value))
                                ) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(
                                    new Error("Se deben ingresar 10 números")
                                );
                            },
                        }),
                    ]}
                >
                    <Input
                        placeholder="Por ejemplo, 1234567890"
                        maxLength={10}
                        type="text"
                        pattern="\d*"
                        title="Only numbers are allowed"
                        style={{ width: "50%" }}
                    />
                </Form.Item>

                <Form.Item
                    name="numberCard"
                    label="Número de tarjeta"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                        () => ({
                            validator(_, value) {
                                if (
                                    !value ||
                                    (value.length === 16 && /^\d+$/.test(value))
                                ) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(
                                    new Error("Se deben ingresar 16 números")
                                );
                            },
                        }),
                    ]}
                >
                    <Input
                        maxLength={16}
                        type="text"
                        style={{ width: "50%" }}
                        pattern="\d*"
                        title="Only numbers are allowed"
                    />
                </Form.Item>
                <Form.Item
                    label="Fecha de vencimiento"
                    style={{
                        marginBottom: 0,
                    }}
                >
                    <Form.Item
                        name="monthCard"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: "Información obligatoria",
                            },
                        ]}
                        style={{
                            display: "inline-block",
                            width: "calc(50% - 8px)",
                        }}
                    >
                        <Select placeholder="Mes">
                            <Option value="01">01</Option>
                            <Option value="02">02</Option>
                            <Option value="03">03</Option>
                            <Option value="04">04</Option>
                            <Option value="05">05</Option>
                            <Option value="06">06</Option>
                            <Option value="07">07</Option>
                            <Option value="08">08</Option>
                            <Option value="09">09</Option>
                            <Option value="10">10</Option>
                            <Option value="11">11</Option>
                            <Option value="12">12</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        name="ageCard"
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: "Información obligatoria",
                            },
                        ]}
                        style={{
                            display: "inline-block",
                            width: "calc(50% - 8px)",
                            margin: "0 8px",
                        }}
                    >
                        <Select placeholder="Año">
                            <Option value="2023">2023</Option>
                            <Option value="2024">2024</Option>
                            <Option value="2025">2025</Option>
                            <Option value="2026">2026</Option>
                            <Option value="2027">2027</Option>
                            <Option value="2028">2028</Option>
                            <Option value="2029">2029</Option>
                            <Option value="2030">2030</Option>
                            <Option value="2031">2031</Option>
                            <Option value="2032">2032</Option>
                        </Select>
                    </Form.Item>
                </Form.Item>

                <Form.Item
                    name="ccv"
                    label="Código de seguridad"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                        () => ({
                            validator(_, value) {
                                if (
                                    !value ||
                                    (value.length === 3 && /^\d+$/.test(value))
                                ) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(
                                    new Error("Se deben ingresar 3 números")
                                );
                            },
                        }),
                    ]}
                >
                    <Input
                        type="text"
                        maxLength={3}
                        pattern="\d*"
                        title="Only numbers are allowed"
                        style={{ width: "25%" }}
                    />
                </Form.Item>

                <Form.Item
                    name="typeSend"
                    label="Tipo de envío"
                    rules={[
                        {
                            required: true,
                            message: "Información obligatoria",
                        },
                    ]}
                >
                    <Radio.Group>
                        <Radio value="public">Normal</Radio>
                        <Radio value="private">Prioritario</Radio>
                    </Radio.Group>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default DireccionPagoModal;
