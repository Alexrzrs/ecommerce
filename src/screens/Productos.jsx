import React, { useEffect, useState } from "react";
import { Flex, Button, Modal } from "antd";
import { Link, useNavigate } from "react-router-dom";
import Buscador from "../components/Buscador";
import ChatBubble from "../components/ChatBubble";
import {
    collection,
    getDocs,
    getDoc,
    doc,
    updateDoc,
    setDoc,
    arrayUnion,
} from "firebase/firestore";
import { Toaster, toast } from "sonner";

import { db } from "../firebaseConfig";
import { ListaProductoItem } from "../components/ListaProductoItem";
import { Recomendaciones } from "../components/Recomendaciones";
import Navbar from "../components/Navbar";
import { useUser } from "../context/UserContext";

export default function Productos() {
    const [products, setProducts] = useState([]);
    const [filteredProducts, setFilteredProducts] = useState([]);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { userData } = useUser();
    const navigate = useNavigate();

    console.log("userData:", userData);

    const handleSearch = (searchText) => {
        const filtered = products.filter((producto) =>
            producto.nombre.toLowerCase().includes(searchText.toLowerCase())
        );
        setFilteredProducts(filtered);
    };

    const addToCart = async (producto) => {
        try {
            if (!userData) {
                toast.error("Inicia sesion para crear un carrito.");
                setIsModalVisible(true); // Show the modal
                return;
            }

            // Obtener el carrito del usuario desde la base de datos
            const userCartRef = doc(db, "carritos", userData.uid);
            const userCartDoc = await getDoc(userCartRef);

            if (userCartDoc.exists()) {
                // Si el carrito ya existe, verificar si el producto está presente
                const existingProductIndex = userCartDoc
                    .data()
                    .productos.findIndex(
                        (cartProduct) => cartProduct.id === producto.id
                    );

                if (existingProductIndex !== -1) {
                    // Si el producto ya está en el carrito, incrementar la cantidad en uno
                    const updatedCartItems = [...userCartDoc.data().productos];
                    updatedCartItems[existingProductIndex].cantidad += 1;
                    // console.log("Producto existe en carrito:", producto);
                    // Actualizar el carrito en la base de datos
                    await updateDoc(userCartRef, {
                        productos: updatedCartItems,
                    });
                } else {
                    // Si el producto no está en el carrito, agregar uno nuevo
                    const updatedCartItems = arrayUnion({
                        ...producto,
                        cantidad: 1,
                    });
                    // console.log("Producto agregado al carrito:", producto);
                    // Actualizar el carrito en la base de datos
                    await updateDoc(userCartRef, {
                        productos: updatedCartItems,
                    });
                }
            } else {
                // Si el carrito no existe, crear uno nuevo con el producto
                await setDoc(userCartRef, {
                    productos: [{ ...producto, cantidad: 1 }],
                });
            }
        } catch (error) {
            console.error(
                "Error al agregar el producto al carrito:",
                error.message
            );
        }
    };
    const getProductos = async () => {
        let productos = [];
        const querySnapshot = await getDocs(collection(db, "productos"));
        querySnapshot.forEach((doc) => {
            const producto = doc.data();
            producto.id = doc.id;
            productos.push(producto);
        });
        // console.log(productos);
        setProducts(productos);
        setFilteredProducts(productos);
    };

    const handleModalOk = () => {
        setIsModalVisible(false);
        navigate("/login");
    };

    const handleModalCancel = () => {
        setIsModalVisible(false);
    };

    const goToPedidos = () => {
        if (userData == null) {
            setIsModalVisible(true)
            toast.error("Inicia sesion para ver pedidos.");
        } else {
            navigate("/pedidos")
        }
    }

    useEffect(() => {
        getProductos();
    }, []);

    return (
        <Flex justify="center" align="center" vertical>
            <Navbar />
            <Toaster richColors position="top-center" />
            <h1>Todos los productos</h1>
            <ChatBubble />
            <Button onClick={goToPedidos} >Ir a mis pedidos</Button>
            <Recomendaciones />
            <Buscador onSearch={handleSearch} />
            <Flex wrap="wrap" gap="large" justify="center">
                {filteredProducts.length === 0
                    ? null
                    : filteredProducts.map((producto) => (
                        <ListaProductoItem
                            producto={producto}
                            addToCart={() => addToCart(producto)}
                            key={producto.id}
                        />
                    ))}
            </Flex>
            <Modal
                title="Autenticación Requerida"
                open={isModalVisible}
                onOk={handleModalOk}
                onCancel={handleModalCancel}
                okText="Iniciar Sesión"
                cancelText="Cancelar"
            >
                <p>Debes iniciar sesión para hacer esta acción.</p>
            </Modal>
        </Flex>
    );
}
