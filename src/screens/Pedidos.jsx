// Importa las librerías necesarias
import React, { useState, useEffect } from "react";
import {
    doc,
    onSnapshot,
    collection,
    query,
    where,
    updateDoc,
    deleteDoc,
} from "firebase/firestore";

import { db } from "../firebaseConfig"; // Asegúrate de importar correctamente tu instancia de Firebase
import { Button, List, Modal as AntModal } from "antd";
import { useUser } from "../context/UserContext";
import Navbar from "../components/Navbar";

// Definición del componente Pedidos
const Pedidos = () => {
    // Estados para los pedidos, pedido seleccionado y estado del modal
    const [orders, setOrders] = useState([]);
    const [selectedOrder, setSelectedOrder] = useState(null);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const { userData } = useUser();

    // Obtener pedidos del usuario desde Firestore
    useEffect(() => {
        // Referencia a la colección de pedidos
        const ordersCollection = collection(db, "pedidos");
        const userOrdersQuery = query(
            ordersCollection,
            where("idUsuario", "==", userData.uid)
        );

        // Escuchar cambios en la colección de pedidos
        const unsubscribe = onSnapshot(userOrdersQuery, (snapshot) => {
            const newOrders = snapshot.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));
            setOrders(newOrders);
        });

        // Limpiar el listener al desmontar el componente
        return () => unsubscribe();
    }, [userData]);

    // Función para abrir el modal con los detalles del pedido
    const openModal = (order) => {
        setSelectedOrder(order);
        setModalIsOpen(true);
    };

    // Función para cerrar el modal
    const closeModal = () => {
        setSelectedOrder(null);
        setModalIsOpen(false);
    };

    // Función para cancelar un pedido
    const cancelOrder = async (orderId) => {
        try {
            // Eliminar el pedido en Firestore
            const orderRef = doc(db, "pedidos", orderId);
            await deleteDoc(orderRef);

            // Eliminar localmente el pedido
            setOrders((prevOrders) =>
                prevOrders.filter((order) => order.id !== orderId)
            );

            // Cerrar el modal
            closeModal();

            console.log(`Pedido cancelado: ${orderId}`);
        } catch (error) {
            console.error("Error al cancelar el pedido:", error.message);
        }
    };

    // Renderizar el componente
    return (
        <>
            <Navbar />
            <div className="container" style={{ padding: 50 }}>
                <h1>Lista de Pedidos</h1>
                {/* Lista de pedidos con acciones y botones */}
                <List
                    itemLayout="horizontal"
                    dataSource={orders}
                    renderItem={(order) => (
                        <List.Item
                            actions={[
                                <Button onClick={() => openModal(order)}>
                                    Ver Detalles
                                </Button>,
                            ]}
                        >
                            <List.Item.Meta
                                title={`Order ID: ${order.id}`}
                                description={`Estado: ${order.estado}`}
                            />
                        </List.Item>
                    )}
                />

                {/* Modal con detalles del pedido */}
                <AntModal
                    title="Detalles del Pedido"
                    visible={modalIsOpen}
                    onCancel={closeModal}
                    footer={[
                        <Button
                            key="cancelar"
                            onClick={() => cancelOrder(selectedOrder.id)}
                        >
                            Cancelar Pedido
                        </Button>,
                        <Button
                            key="cerrar"
                            type="primary"
                            onClick={closeModal}
                        >
                            Cerrar
                        </Button>,
                    ]}
                >
                    {/* Contenido del modal con detalles del pedido */}
                    {selectedOrder && (
                        <div>
                            <h3>Productos</h3>
                            <List
                                dataSource={selectedOrder.productos}
                                renderItem={(producto) => (
                                    <List.Item>
                                        <List.Item.Meta
                                            title={producto.titulo}
                                            description={`Precio: $${producto.precio
                                                }, Cantidad: ${producto.cantidad
                                                }, Subtotal: $${producto.cantidad *
                                                producto.precio
                                                }`}
                                        />
                                    </List.Item>
                                )}
                            />
                            <h3>Datos de Envío</h3>
                            <p>Nombre: {selectedOrder.nombre}</p>
                            <p>Dirección: {selectedOrder.direccion}</p>
                            <p>Teléfono: {selectedOrder.telefono}</p>
                            <h3>Datos de Pago</h3>
                            <p>Tarjeta: {selectedOrder.numeroTarjeta}</p>
                            <p>Subtotal: ${selectedOrder.subtotal}</p>
                            <p>Cupón: {selectedOrder.cupon}</p>
                            <p>Monto Total: ${selectedOrder.total}</p>
                        </div>
                    )}
                </AntModal>
            </div>
        </>
    );
};

export default Pedidos;
