import React, { useState } from "react";
import { Modal, Button, Input, Alert } from "antd";
import { TagOutlined } from "@ant-design/icons";

const CouponModal = ({ visible, onCancel, onApply }) => {
    const [couponCode, setCouponCode] = useState("");
    const [error, setError] = useState(false);

    const handleApplyCoupon = () => {
        if (couponCode === "DISCOUNT10") {
            setError(false);
            onApply(couponCode);
            setCouponCode("");
        } else {
            setError(true);
        }
    };

    return (
        <Modal
            title="Ingresar Cupón de Descuento"
            open={visible}
            onCancel={onCancel}
            footer={[
                <Button key="cancel" onClick={onCancel}>
                    Cancelar
                </Button>,
            ]}
        >
            <Input.Group compact style={{ marginBottom: "20px" }}>
                <Input
                    prefix={
                        <TagOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    placeholder="Ingrese el cupón de descuento"
                    value={couponCode}
                    onChange={(e) => setCouponCode(e.target.value)}
                    style={{ width: "80%" }}
                />
                <Button
                    type="primary"
                    style={{ width: "20%" }}
                    onClick={handleApplyCoupon}
                >
                    Aplicar
                </Button>
            </Input.Group>
            <p style={{ marginBottom: "10px" }}>DISCOUNT10</p>
            {error && (
                <Alert
                    message="El cupón ingresado no es válido"
                    type="error"
                    showIcon
                    style={{ marginBottom: "10px" }}
                />
            )}
        </Modal>
    );
};

export default CouponModal;
