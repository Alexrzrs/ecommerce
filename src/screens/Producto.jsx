import React, { useEffect, useState, useRef } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import "./Producto.css";
import {
    Avatar,
    Breadcrumb,
    Button,
    Card,
    Carousel,
    Col,
    Flex,
    List,
    Row,
    Rate,
    Input,
    Typography,
    Modal,
} from "antd";
import { db } from "../firebaseConfig";
import {
    doc,
    addDoc,
    collection,
    getDoc,
    getDocs,
    query,
    where,
    updateDoc,
    setDoc,
    arrayUnion,
} from "firebase/firestore";
import { useUser } from "../context/UserContext";
import Navbar from "../components/Navbar";
import { Toaster, toast } from "sonner";

export default function Producto() {
    const { id } = useParams();
    const [producto, setProducto] = useState(null);
    const [calificacion, setCalificacion] = useState(0);
    const [resena, setResena] = useState("");
    const [mostrarFormulario, setMostrarFormulario] = useState(true);
    const listaResenasRef = useRef(null);
    const [reseñas, setReseñas] = useState([]);
    const [calificacionPromedio, setCalificacionPromedio] = useState(0);
    const { userData } = useUser();
    const [isModalVisible, setIsModalVisible] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        const getProduct = async () => {
            const docRef = doc(db, "productos", id);
            const docSnap = await getDoc(docRef);
            const product = docSnap.data();
            product.id = docSnap.id;
            setProducto(product);
        };

        getProduct();
    }, [id]);

    const handleModalOk = () => {
        setIsModalVisible(false);
        navigate("/login");
    };

    const handleModalCancel = () => {
        setIsModalVisible(false);
    };

    const addToCart = async (producto) => {
        try {
            if (!userData) {
                toast.error("Inicia sesion para crear un carrito.");
                setIsModalVisible(true); // Show the modal
                return;
            }

            // Obtener el carrito del usuario desde la base de datos
            const userCartRef = doc(db, "carritos", userData.uid);
            const userCartDoc = await getDoc(userCartRef);

            if (userCartDoc.exists()) {
                // Si el carrito ya existe, verificar si el producto está presente
                const existingProductIndex = userCartDoc
                    .data()
                    .productos.findIndex(
                        (cartProduct) => cartProduct.id === producto.id
                    );

                if (existingProductIndex !== -1) {
                    // Si el producto ya está en el carrito, incrementar la cantidad en uno
                    const updatedCartItems = [...userCartDoc.data().productos];
                    updatedCartItems[existingProductIndex].cantidad += 1;
                    // console.log("Producto existe en carrito:", producto);
                    // Actualizar el carrito en la base de datos
                    await updateDoc(userCartRef, {
                        productos: updatedCartItems,
                    });
                } else {
                    // Si el producto no está en el carrito, agregar uno nuevo
                    const updatedCartItems = arrayUnion({
                        ...producto,
                        cantidad: 1,
                    });
                    // console.log("Producto agregado al carrito:", producto);
                    // Actualizar el carrito en la base de datos
                    await updateDoc(userCartRef, {
                        productos: updatedCartItems,
                    });
                }
            } else {
                // Si el carrito no existe, crear uno nuevo con el producto
                await setDoc(userCartRef, {
                    productos: [{ ...producto, cantidad: 1 }],
                });
            }
        } catch (error) {
            console.error(
                "Error al agregar el producto al carrito:",
                error.message
            );
        }
    };

    const handleCalificar = async () => {
        if (mostrarFormulario) {
            const nuevaResena = {
                titulo: "Nueva",
                descripcion: resena,
                calificacion,
                idProducto: id,
                idUser: userData.uid,
                nombre: userData.email,
            };

            // Guardar reseña en Firebase
            const reseñasCollection = collection(db, "resenas");
            await addDoc(reseñasCollection, nuevaResena);

            // Actualizar el estado de las reseñas
            setMostrarFormulario(false);
            obtenerReseñas();
        }
    };

    const obtenerReseñas = async () => {
        try {
            // Realiza una consulta para obtener solo las reseñas del producto actual
            const resenasCollection = collection(db, "resenas");
            const querySnapshot = await getDocs(
                query(resenasCollection, where("idProducto", "==", id))
            );

            // Mapea los documentos de reseñas y actualiza el estado
            const resenasData = querySnapshot.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));
            setReseñas(resenasData);

            // Calcula el promedio de las calificaciones
            const totalCalificaciones = resenasData.reduce(
                (total, reseña) => total + reseña.calificacion,
                0
            );
            const promedio = totalCalificaciones / resenasData.length || 0;
            setCalificacionPromedio(promedio);
        } catch (error) {
            console.error("Error al obtener reseñas:", error);
        }
    };

    useEffect(() => {
        obtenerReseñas();
    }, []);

    const BreacrumbTitle = (
        <>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link to="/">Productos</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {producto && producto.categoria}
                </Breadcrumb.Item>
                <Breadcrumb.Item>{producto && producto.nombre}</Breadcrumb.Item>
            </Breadcrumb>
        </>
    );

    return (
        <>
            <Navbar />
            <Toaster richColors position="top-center" />
            <Flex justify="center" vertical style={{ padding: 10 }}>
                {producto == null ? null : (
                    <>
                        <br />
                        <Card title={BreacrumbTitle}>
                            <Row>
                                <Col xs={24} md={8} xl={8}>
                                    <Carousel autoplay>
                                        {producto.imagenes.map(
                                            (imagen, index) => (
                                                <div
                                                    className="img-container"
                                                    key={index}
                                                >
                                                    <img
                                                        alt=""
                                                        src={imagen}
                                                        width={200}
                                                    />
                                                </div>
                                            )
                                        )}
                                    </Carousel>
                                </Col>
                                <Col xs={24} md={16} xl={16}>
                                    <Typography.Title>
                                        {producto.nombre}
                                    </Typography.Title>
                                    <Rate
                                        disabled
                                        allowHalf
                                        defaultValue={calificacionPromedio}
                                    />
                                    <Row>
                                        <Col xs={24} md={24} lg={18}>
                                            <Flex
                                                justify="space-between"
                                                align="center"
                                                style={{ marginRight: 50 }}
                                            >
                                                <Typography.Paragraph strong>
                                                    Disponible(s):{" "}
                                                    {producto.stock}
                                                </Typography.Paragraph>
                                                <Typography.Title level={3}>
                                                    ${producto.precio} MXN
                                                </Typography.Title>
                                            </Flex>
                                            <Typography.Paragraph>
                                                Acerca de:
                                            </Typography.Paragraph>
                                            <Typography.Paragraph>
                                                {producto.descripcion}
                                            </Typography.Paragraph>
                                        </Col>
                                        <Col xs={24} md={24} lg={6}>
                                            <Flex
                                                vertical
                                                justify="space-evenly"
                                                align="center"
                                                style={{
                                                    height: "100%",
                                                    padding: 10,
                                                }}
                                            >
                                                <Button
                                                    block
                                                    type="primary"
                                                    style={{ marginBottom: 10 }}
                                                    onClick={() =>
                                                        addToCart(producto)
                                                    }
                                                >
                                                    Agregar al carrito
                                                </Button>
                                            </Flex>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card>
                        <Flex vertical style={{ padding: 50 }}>
                            <Typography.Title level={2}>
                                Reseñas
                            </Typography.Title>
                            {/* Formulario de calificación y reseña */}
                            {mostrarFormulario && (
                                <>
                                    <Rate
                                        value={calificacion}
                                        onChange={(value) =>
                                            setCalificacion(value)
                                        }
                                    />
                                    <Input
                                        type="textarea"
                                        rows={4}
                                        value={resena}
                                        onChange={(e) =>
                                            setResena(e.target.value)
                                        }
                                    />
                                    <Button
                                        type="primary"
                                        onClick={handleCalificar}
                                    >
                                        Calificar y dejar reseña
                                    </Button>
                                </>
                            )}
                            {/* Lista de reseñas */}
                            <List
                                itemLayout="horizontal"
                                dataSource={reseñas}
                                renderItem={(item, index) => (
                                    <List.Item key={index}>
                                        <List.Item.Meta
                                            avatar={
                                                <Avatar
                                                    src={`https://xsgames.co/randomusers/avatar.php?g=pixel&key=${index}`}
                                                />
                                            }
                                            title={
                                                <>
                                                    <a href="https://ant.design">
                                                        {item.nombre}
                                                    </a>
                                                    {/* Muestra la calificación como estrellas */}
                                                    <Rate
                                                        disabled
                                                        defaultValue={
                                                            item.calificacion
                                                        }
                                                    />
                                                </>
                                            }
                                            description={item.descripcion}
                                        />
                                    </List.Item>
                                )}
                                ref={listaResenasRef}
                            />
                            {/* Muestra la calificación promedio */}
                        </Flex>
                    </>
                )}
            </Flex>
            <Modal
                title="Autenticación Requerida"
                open={isModalVisible}
                onOk={handleModalOk}
                onCancel={handleModalCancel}
                okText="Iniciar Sesión"
                cancelText="Cancelar"
            >
                <p>Debes iniciar sesión para agregar productos al carrito.</p>
            </Modal>
        </>
    );
}
