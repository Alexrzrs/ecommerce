import React, { createContext, useContext, useState } from "react";

const UserContext = createContext();

export const UserProvider = ({ children }) => {
    // Intenta recuperar datos de usuario del localStorage al cargar la aplicación
    const storedUserData = localStorage.getItem("userData");
    const initialUserData = storedUserData ? JSON.parse(storedUserData) : null;

    const [userData, setUserDataState] = useState(initialUserData);

    const setUser = (newUserData) => {
        // Almacenar en localStorage
        localStorage.setItem("userData", JSON.stringify(newUserData));

        // Establecer en el estado del contexto
        setUserDataState(newUserData);
    };

    return (
        <UserContext.Provider value={{ userData, setUser }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => {
    return useContext(UserContext);
};
